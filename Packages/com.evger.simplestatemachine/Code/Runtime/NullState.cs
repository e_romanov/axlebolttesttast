using Cysharp.Threading.Tasks;

namespace SimpleStateMachine
{
#pragma warning disable 1998
    public class NullState : IState
    {

        public async UniTask OnEnter()
        {
            
        }
        
        public async UniTask OnExit()
        {
            
        }
    }
#pragma warning restore 1998
}