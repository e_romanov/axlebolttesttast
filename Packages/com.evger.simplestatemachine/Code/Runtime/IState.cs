using Cysharp.Threading.Tasks;

namespace SimpleStateMachine
{
    public interface IState
    {
        UniTask OnEnter();
        UniTask OnExit();
    }
}