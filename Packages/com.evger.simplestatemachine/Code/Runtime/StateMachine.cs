using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;

namespace SimpleStateMachine
{
    public class StateMachine
    {
        protected readonly Dictionary<Type, IState> States;
        public IState ActiveState { get; protected set;}

        public StateMachine()
        {
            States = new Dictionary<Type, IState>();
            ActiveState = new NullState();
        }

        public StateMachine RegisterStates(IState[] states)
        {
            for (int i = 0; i < states.Length; i++)
            {
                States.Add(states[i].GetType(), states[i]);
            }
            return this;
        }

        public async UniTask SetActiveState<T>() where T: IState
        {
            Type stateType = typeof(T);
            if (!States.ContainsKey(stateType))
            {
                UnityEngine.Debug.LogError($"State of type {stateType} is not present in state machine");
                return;
            }
            
            // check if entering state first time
            if(!(ActiveState is NullState))
                await ActiveState.OnExit();
            
            ActiveState = States[stateType];
            await ActiveState.OnEnter();
        }
    }
}