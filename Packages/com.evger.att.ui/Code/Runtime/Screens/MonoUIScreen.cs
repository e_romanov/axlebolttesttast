using Cysharp.Threading.Tasks;
using UnityEngine;

namespace ATT.UI.Screens
{
    // base class for monobehaviour based screens, may be used as common interface for referencing in serialized fields
    public abstract class MonoUIScreen : MonoBehaviour, IUIScreen
    {
        [SerializeField] private int _screenID;
        public virtual int ScreenID => _screenID;
        public abstract UniTask Show();

        public abstract UniTask Hide();
    }
}