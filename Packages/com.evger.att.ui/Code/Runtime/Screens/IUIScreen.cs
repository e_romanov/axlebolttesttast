using Cysharp.Threading.Tasks;

namespace ATT.UI.Screens
{
    public interface IUIScreen
    {
        int ScreenID { get; }
        UniTask Show();
        UniTask Hide();
    }
}