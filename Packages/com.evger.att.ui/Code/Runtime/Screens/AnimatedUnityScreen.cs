using System;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace ATT.UI.Screens
{
    
    // just basic animated screen, plays unity animation during screen switching process
    // under current task context no need to build some complex custom screens management and animation system
    [RequireComponent(typeof(Canvas), typeof(Animator), typeof(GraphicRaycaster))]
    public class AnimatedUnityScreen : MonoUIScreen
    {
        [System.Serializable]
        public struct AnimationData
        {
            [SerializeField] public string AnimationKey;
            [SerializeField] public float AnimationDuration;
        }
        
        [SerializeField, HideInInspector] private Canvas _canvas;
        [SerializeField, HideInInspector] private GraphicRaycaster _raycaster;
        [SerializeField, HideInInspector] private Animator _animator;

        [Header("Animation")] 
        [SerializeField] private AnimationData _showAnimation;
        [SerializeField] private AnimationData _hideAnimation;
        

        void Awake()
        {
            // in case if components was not serialized
            TryReFetchComponents();
        }
        
        public override async UniTask Show()
        {
            _canvas.enabled = true;
            await PlayAndWaitForAnimation(_animator, _showAnimation);
            _raycaster.enabled = true;
        }

        public override async UniTask Hide()
        {
            _raycaster.enabled = false;
            await PlayAndWaitForAnimation(_animator, _hideAnimation);
            _canvas.enabled = false;
        }

        private async UniTask PlayAndWaitForAnimation(Animator _animator, AnimationData animationData)
        {
            _animator.enabled = true;
            _animator.Play(animationData.AnimationKey, 0, 0.0f);
            await UniTask.Delay(TimeSpan.FromSeconds(animationData.AnimationDuration));
            _animator.enabled = false;
        }

        private void TryReFetchComponents()
        {
            if (_canvas is null)
                _canvas = GetComponent<Canvas>();

            if (_raycaster is null)
                _raycaster = GetComponent<GraphicRaycaster>();

            if (_animator is null)
                _animator = GetComponent<Animator>();
        }

        
#if UNITY_EDITOR
        private void OnValidate()
        {
            TryReFetchComponents();
        }
#endif
    }
}