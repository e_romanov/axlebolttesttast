using Cysharp.Threading.Tasks;

namespace ATT.UI.Screens
{
    public interface IScreenSwitcher
    {
        UniTask SetScreen(int screenID);
        IUIScreen CurrentScreen { get; }
        IScreenSwitcher RegisterScreen(IUIScreen screen);
    }
}