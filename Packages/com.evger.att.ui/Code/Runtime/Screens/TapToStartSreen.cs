using System;
using UnityEngine;

namespace ATT.UI.Screens
{
    public class TapToStartSreen : AnimatedUnityScreen
    {
        [SerializeField] private UnityEngine.UI.Button _startButton;
        public event Action OnClickStart;

        private void OnEnable()
        {
            _startButton.onClick.AddListener(InvokeOnClickStart);
        }

        private void OnDisable()
        {
            _startButton.onClick.RemoveListener(InvokeOnClickStart);
        }

        private void InvokeOnClickStart()
        {
            OnClickStart?.Invoke();
        }
    }
}