using System.Collections.Generic;
using Cysharp.Threading.Tasks;

namespace ATT.UI.Screens
{
    public class ScreenSwitcher : IScreenSwitcher
    {
        private Dictionary<int, IUIScreen> _screens;
        public IUIScreen CurrentScreen { get; private set; }

        public ScreenSwitcher(Dictionary<int, IUIScreen> screens)
        {
            _screens = screens;
        }

        public ScreenSwitcher()
        {
            _screens = new Dictionary<int, IUIScreen>();
        }

        public async UniTask SetScreen(int screenID)
        {
            if (!_screens.ContainsKey(screenID))
            {
                UnityEngine.Debug.LogError($"Screen with screenID: ${screenID} not found, aborting screen switch");
                return;
            }
            
            if (!(CurrentScreen is null))
                CurrentScreen.Hide().Forget(); // to show next screen without waiting of prev screen hiding

            CurrentScreen = _screens[screenID];
            await CurrentScreen.Show();
        }
        
        public IScreenSwitcher RegisterScreen(IUIScreen screen)
        {
            if (_screens.ContainsKey(screen.ScreenID))
            {
                UnityEngine.Debug.LogError($"Cant register screens with the same IDS. ConfictingID{screen.ScreenID} Aborting.");
                return this;
            }
            
            _screens.Add(screen.ScreenID, screen);

            return this;
        }
    }
}