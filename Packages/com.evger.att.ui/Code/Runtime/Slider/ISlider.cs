namespace ATT.UI.Slider
{
    public interface ISlider
    {
        public float NormalizedValue { get; set;}
    }
}