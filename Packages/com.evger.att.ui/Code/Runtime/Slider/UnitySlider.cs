namespace ATT.UI.Slider
{
    public class UnitySlider : UnityEngine.UI.Slider, ISlider
    {
        public float NormalizedValue { get => this.normalizedValue; set => this.normalizedValue = value;}
    }
}