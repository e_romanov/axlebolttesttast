namespace ATT.UI.Slider
{
    public class ImageFillAmountSlider : UnityEngine.UI.Image, ISlider
    {
        public float NormalizedValue { get => this.fillAmount; set => this.fillAmount = value;}
    }
}