using ATT.Simulation.Mono.AI.Ball;
using ATT.Simulation.Mono.AI.Ball.States;
using ATT.Simulation.Mono.Physics.WorldBorder;
using ATT.Simulation.SimulationBackend;

namespace ATT.Simulation.Mono
{
    public class MonoSimulationBackend : ISimulationBackend
    {
        private IPhysicsBordersProvider _physicsBordersProvider;
        private BallAI _ballAI;

        public MonoSimulationBackend(IPhysicsBordersProvider physicsBordersProvider, BallAI ballAI)
        {
            _physicsBordersProvider = physicsBordersProvider;
            _ballAI = ballAI;
        }

        public void StartSimulation()
        {
            _ballAI.SetState<MovingToTargetState>();
        }

        public void FrameUpdate(float deltaTime)
        {
            // if more frame update components will be required, define common interface for them and use in group
            _physicsBordersProvider.UpdateBorders();
        }
        
        public void Dispose()
        {
            return;
        }
    }
}