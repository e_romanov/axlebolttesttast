using ATT.Simulation.Configuration;
using ATT.Simulation.Controlls;
using ATT.Simulation.Data;
using ATT.Simulation.Mono.AI.Ball;
using ATT.Simulation.Mono.AI.Ball.Data;
using ATT.Simulation.Mono.Borders;
using ATT.Simulation.Mono.Cursor;
using ATT.Simulation.Mono.Physics;
using ATT.Simulation.Mono.Physics.WorldBorder;
using ATT.Simulation.SimulationBackend;
using ATT.Simulation.UnityComponents;
using UnityEngine;

namespace ATT.Simulation.Mono
{
    // configures objects for mono simulation and creates simulation backend
    public class MonoSimulationBackendFactory : ISimulationBackendFactory
    {
        private EnvironmentConfig _environmentConfig;
        private BallConfig _ballConfig;
        private CursorConfig _cursorConfig;
        private ControllsGroup _controllsGroup;
        private SceneReferences _sceneReferences;

        public MonoSimulationBackendFactory(EnvironmentConfig environmentConfig, BallConfig ballConfig, CursorConfig cursorConfig, ControllsGroup controllsGroup, SceneReferences sceneReferences)
        {
            _environmentConfig = environmentConfig;
            _ballConfig = ballConfig;
            _cursorConfig = cursorConfig;
            _controllsGroup = controllsGroup;
            _sceneReferences = sceneReferences;
        }

        public ISimulationBackend CreateBackend()
        {
            IBordersProvider startFinishBordersProvider = new BordersHolder(_sceneReferences.FinishBorder, _sceneReferences.StartBorder);
            IPhysicsBordersProvider physicsBordersProvider = CreatePhysicsBorderProvider(_sceneReferences.PhysicsBorders);
            
            Cursor.Cursor cursor = CreateCursor(_environmentConfig.CursorView, _cursorConfig, _controllsGroup.CursorSizeSlider,
                _controllsGroup.Cursor, _environmentConfig.CursorDisplacementRaycastMask,
                _environmentConfig.MaxRaycastDistance);
            ICursorProvider cursorProvider = new CursorHolder(cursor);
            
            
            BallAI ballAI = CreateBall(_environmentConfig.BallViewPrefab, physicsBordersProvider, startFinishBordersProvider,
                cursorProvider, _controllsGroup.AccelerationSlider ,_controllsGroup.VelocitySlider,_ballConfig, _environmentConfig.BallSafeDistance);
            ballAI.transform.position = _sceneReferences.StartBorder.transform.position;
            
            return new MonoSimulationBackend(physicsBordersProvider, ballAI);
        }

        public static IPhysicsBordersProvider CreatePhysicsBorderProvider(PhysicsBorderView[] borderViews)
        {
            TransformBasedPhysicsBordersProvider result = new TransformBasedPhysicsBordersProvider();
            for (int i = 0; i < borderViews.Length; i++)
            {
                result.AddBorder(borderViews[i].transform);
            }
            result.UpdateBorders();
            return result;
        }
        
        // can be moved to separate IBallFactory if customisable ball creation behaviour will be needed 
        public static BallAI CreateBall(BallView ballView, IPhysicsBordersProvider physicsBorderProvider,
            IBordersProvider startFinishBordersProvider,ICursorProvider cursorProvider, ISlider acceleartionSlider, ISlider speedSlider, BallConfig ballConfig, float ballAISafeDistance)
        {
            BallView view = Object.Instantiate(ballView);
            
            PhysicsObject physicsObject = view.gameObject.AddComponent<PhysicsObject>();
            physicsObject.Setup(physicsBorderProvider);

            BallAIData ballAIData = new BallAIData(ballConfig, ballAISafeDistance);

            return view.gameObject
                .AddComponent<BallAI>()
                .Setup(physicsObject, cursorProvider, acceleartionSlider, speedSlider, startFinishBordersProvider, ballAIData);
        }

        // can be moved to separate IMonoCursorFactory if customisable cursor creation behaviour will be needed 
        public static Cursor.Cursor CreateCursor(CursorView cursorView, CursorConfig cursorConfig, ISlider cursorSizeSlider,
            ICursor cursor, int cursorDisplacementRaycastMask, float maxRaycastDistance)
        {
            CursorData cursorData = new CursorData(cursorConfig.MinScale, cursorConfig.MinScale, cursorConfig.MaxScale);
            return Object
                .Instantiate(cursorView)
                .gameObject.AddComponent<Cursor.Cursor>()
                .Setup(cursorData, cursorSizeSlider, cursor, cursorDisplacementRaycastMask, maxRaycastDistance);
        }
        
    }
}