using UnityEngine;

namespace ATT.Simulation.Mono.Cursor
{
    public interface ICursorProvider
    {
        public Cursor Cursor { get; }
        public Vector2 CursorPosition { get; }
        public float CursorSize { get; }
    }
}