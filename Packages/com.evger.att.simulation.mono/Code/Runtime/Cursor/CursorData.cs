namespace ATT.Simulation.Mono.Cursor
{
    public class CursorData
    {
        public float CursorSize;
        public float MinSize;
        public float MaxSize;

        public CursorData(float cursorSize, float minSize, float maxSize)
        {
            CursorSize = cursorSize;
            MinSize = minSize;
            MaxSize = maxSize;
        }

        public CursorData()
        {
            CursorSize = 1f;
            MinSize = 1f;
            MaxSize = 1f;
        }
    }
}