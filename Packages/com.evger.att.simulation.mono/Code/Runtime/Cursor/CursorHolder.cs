using UnityEngine;

namespace ATT.Simulation.Mono.Cursor
{
    public class CursorHolder : ICursorProvider
    {
        private Cursor _cursor;

        public Cursor Cursor => _cursor;
        public Vector2 CursorPosition => new Vector2(_cursor.Transform.position.x, _cursor.Transform.position.z);
        public float CursorSize => _cursor.CursorSize;

        public CursorHolder(Cursor cursor)
        {
            _cursor = cursor;
        }
    }
}