using ATT.Controls;
using ATT.Simulation.Code.Runtime.Controlls;
using ATT.Simulation.Controlls;
using UnityEngine;

namespace ATT.Simulation.Mono.Cursor
{
    public class Cursor : MonoBehaviour
    {
        [SerializeField] private CursorData _cursorData = new CursorData();
        [SerializeField] private float _maxRayCastDistance;

        private ISlider _slider = new AlwaysMaxSlider();
        private ICursor _cursor = new UnityCursor();
        private Transform _transform;
        private int _raycastMask;
        private Camera _rayCastCamera;
        
        public float CursorSize => _cursorData.CursorSize;
        public Transform Transform => _transform;

        public Cursor Setup(CursorData cursorData, ISlider slider, ICursor cursor, int raycastMask, float maxRayCastDistance)
        {
            _cursorData = cursorData;
            _cursor = cursor;
            _slider = slider;
            _raycastMask = raycastMask;
            _maxRayCastDistance = maxRayCastDistance;
            
            _rayCastCamera = Camera.main;
            _transform = transform;
            return this;
        }

        private void Awake()
        {
            _transform = transform;
            _rayCastCamera = Camera.main;
        }

        private void Update()
        {
            UpdateCursorSize(_cursorData, _slider);
            ScaleTransformToCursorSize(transform, _cursorData);
            if (TryGetNewPosition(out var newPosition, _cursor, _maxRayCastDistance, _raycastMask, _rayCastCamera))
            {
                // use lerp to do some neat smoothing, better to remove magical number 30.0f in the future
                _transform.position = Vector3.Lerp(_transform.position, newPosition, Time.deltaTime * 30.0f); 
            }
        }

        // changes cursor size based on slider value
        public static void UpdateCursorSize(CursorData cursorData, ISlider slider)
        {
            cursorData.CursorSize = Mathf.Lerp(cursorData.MinSize, cursorData.MaxSize, slider.NormalizedValue);
        }

        public static void ScaleTransformToCursorSize(Transform transform, CursorData cursorData)
        {
            transform.localScale = Vector3.one * cursorData.CursorSize;
        }

        public bool TryGetNewPosition(out Vector3 newPosition, ICursor cursor, float maxRayCastDistance,
            int raycastMask, Camera targetCamera)
        {
            newPosition = Vector3.zero;
            
            if (!UnityEngine.Physics.Raycast(targetCamera.ScreenPointToRay(cursor.ScreenPosition),
                out var hitResult, maxRayCastDistance, raycastMask)) return false;

            newPosition = hitResult.point;
            return true;
        }
    }
}