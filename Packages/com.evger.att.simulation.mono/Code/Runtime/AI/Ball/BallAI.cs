using System;
using ATT.Simulation.Code.Runtime.Controlls;
using ATT.Simulation.Controlls;
using ATT.Simulation.Mono.AI.Ball.Data;
using ATT.Simulation.Mono.AI.Ball.States;
using ATT.Simulation.Mono.Borders;
using ATT.Simulation.Mono.Cursor;
using ATT.Simulation.Mono.Physics;
using SimpleStateMachine;
using UnityEngine;

namespace ATT.Simulation.Mono.AI.Ball
{
    public class BallAI : MonoBehaviour
    {
        [SerializeField] private BallAIData _ballAIData = new BallAIData();

        public BallAIData BallAIData => _ballAIData;
        public Vector2 Position => new Vector2(transform.position.x, transform.position.z);
        public event Action OnUpdate;
        
        private StateMachine ballSM;
        private ISlider _accelerationSlier = new AlwaysMaxSlider();
        private ISlider _speedSlider = new AlwaysMaxSlider();
        private StateMachine _stateMachine;

        public BallAI Setup(PhysicsObject physicsObject, ICursorProvider cursorProvider, ISlider accelerationSlider, ISlider speedSlider, IBordersProvider bordersProvider, BallAIData ballAIData)
        {
            _ballAIData = ballAIData;
            _accelerationSlier = accelerationSlider;
            _speedSlider = speedSlider;
            _stateMachine = CreateBallStateMachine(physicsObject, cursorProvider, bordersProvider, this);
            return this;
        }

        public void SetState<T>() where T:IState
        {
            _stateMachine.SetActiveState<T>();;
        }

        void Update()
        {
            UpdateBallAcceleration(_ballAIData, _accelerationSlier);
            UpdateBallTargetSpeed(_ballAIData, _speedSlider);
            OnUpdate?.Invoke();
        }

        public static void UpdateBallAcceleration(BallAIData data, ISlider slider)
        {
            data.CurrentAcceleration = Mathf.Lerp(data.MinAcceleration, data.MaxAcceleration, slider.NormalizedValue);
        }
        
        public static void UpdateBallTargetSpeed(BallAIData data, ISlider slider)
        {
            data.TargetVelocity = Mathf.Lerp(data.MinTargetVelocity, data.MaxTargetVelocity, slider.NormalizedValue);
        }

        public static StateMachine CreateBallStateMachine(PhysicsObject physicsObject, ICursorProvider cursorProvider, IBordersProvider bordersProvider, BallAI ballAI)
        {
            StateMachine result = new StateMachine();
            result.RegisterStates(new IState[]
            {
                new AvoidingState(ballAI, physicsObject, cursorProvider, result),
                new MovingToTargetState(ballAI, physicsObject, cursorProvider, bordersProvider, result)
            });
            return result;
        }
    }
}