using ATT.Simulation.Configuration;

namespace ATT.Simulation.Mono.AI.Ball.Data
{
    [System.Serializable]
    public class BallAIData
    {
        public float CurrentAcceleration;
        public float MinAcceleration;
        public float MaxAcceleration;
        
        public float TargetVelocity;
        public float MinTargetVelocity;
        public float MaxTargetVelocity;
        
        public float AdditionalAvoidingDistance;

        public BallAIData(float currentAcceleration, float minAcceleration, float maxAcceleration, float targetVelocity, float minTargetVelocity, float maxTargetVelocity, float additionalAvoidingDistance)
        {
            CurrentAcceleration = currentAcceleration;
            MinAcceleration = minAcceleration;
            MaxAcceleration = maxAcceleration;
            TargetVelocity = targetVelocity;
            MinTargetVelocity = minTargetVelocity;
            MaxTargetVelocity = maxTargetVelocity;
            AdditionalAvoidingDistance = additionalAvoidingDistance;
        }

        public BallAIData(BallConfig ballConfig, float additionalAvoidingDistance)
        {
            CurrentAcceleration = ballConfig.MinAcceleration;
            TargetVelocity = ballConfig.MinTargetVelocity;
            MaxAcceleration = ballConfig.MaxAcceleration;
            MinAcceleration = ballConfig.MinAcceleration;
            MaxTargetVelocity = ballConfig.MaxTargetVelocity;
            MinTargetVelocity = ballConfig.MinTargetVelocity;
            AdditionalAvoidingDistance = additionalAvoidingDistance;
        }

        public BallAIData()
        {
            CurrentAcceleration = 0f;
            TargetVelocity = 1f;
            MaxAcceleration = 1f;
            MinAcceleration = 0f;
            AdditionalAvoidingDistance = 0f;
            MinTargetVelocity = 1.0f;
            MaxTargetVelocity = 2.0f;
        }
    }
}