using ATT.Simulation.Mono.AI.Ball.Data;
using ATT.Simulation.Mono.Borders;
using ATT.Simulation.Mono.Cursor;
using ATT.Simulation.Mono.Physics;
using Cysharp.Threading.Tasks;
using SimpleStateMachine;
using UnityEngine;

namespace ATT.Simulation.Mono.AI.Ball.States
{
    public class MovingToTargetState : IState
    {
        private BallAI _ballAI;
        private PhysicsObject _physicsObject;
        private ICursorProvider _cursorProvider;
        private IBordersProvider _bordersProvider;
        private StateMachine _stateMachine;

        public MovingToTargetState(BallAI ballAI, PhysicsObject physicsObject, ICursorProvider cursorProvider, IBordersProvider bordersProvider, StateMachine stateMachine)
        {
            _ballAI = ballAI;
            _physicsObject = physicsObject;
            _cursorProvider = cursorProvider;
            _bordersProvider = bordersProvider;
            _stateMachine = stateMachine;
        }

        public async UniTask OnEnter()
        {
            _ballAI.OnUpdate += Update;
        }

        public void Update()
        {;
            if (CanExitState(_cursorProvider.CursorPosition, _ballAI.Position,
                _ballAI.BallAIData.AdditionalAvoidingDistance, _cursorProvider.CursorSize))
            {
                _stateMachine.SetActiveState<AvoidingState>();
                return;
            }

            _physicsObject.CurrentAcceleration = CalculateAcceleration(_ballAI.Position,
                _bordersProvider.FinishBorder.Position, _physicsObject.CurrentSpeed, _ballAI.BallAIData);
        }

        public async UniTask OnExit()
        {
            _ballAI.OnUpdate -= Update;
        }

        public static bool CanExitState(Vector2 cursorPosition, Vector2 ballPosition, float additionalDistanceOffset, float cursorSize)
        {
            return AIUtils.InCursorAvoidingRange(cursorPosition, ballPosition, additionalDistanceOffset, cursorSize);
        }
        
        public static Vector2 CalculateAcceleration(Vector2 ballPosition, Vector2 borderPosition, Vector2 currentSpeed, BallAIData ballAIData)
        {
            Vector2 direction = (borderPosition - ballPosition);
            // if close enough compensate speed, better to remove magical numbers in future
            direction = direction.sqrMagnitude < 0.35f ? -currentSpeed.normalized : direction.normalized;
            
            Vector2 speedDelta =  (direction * ballAIData.TargetVelocity) - currentSpeed;
            
            if(speedDelta.sqrMagnitude < 0.1f)
                return Vector2.zero;

            if (speedDelta.magnitude * Time.deltaTime < ballAIData.CurrentAcceleration * Time.deltaTime) 
                return speedDelta;

            return speedDelta.normalized * ballAIData.CurrentAcceleration;
        }
    }
}