using ATT.Simulation.Mono.Cursor;
using ATT.Simulation.Mono.Physics;
using Cysharp.Threading.Tasks;
using SimpleStateMachine;
using UnityEngine;

namespace ATT.Simulation.Mono.AI.Ball.States
{
    public class AvoidingState : IState
    {
        private BallAI _ballAI;
        private PhysicsObject _physicsObject;
        private ICursorProvider _cursorProvider;
        private StateMachine _stateMachine;

        public AvoidingState(BallAI ballAI, PhysicsObject physicsObject, ICursorProvider cursorProvider, StateMachine stateMachine)
        {
            _ballAI = ballAI;
            _physicsObject = physicsObject;
            _cursorProvider = cursorProvider;
            _stateMachine = stateMachine;
        }

        public async UniTask OnEnter()
        {
            _ballAI.OnUpdate += Update;
        }

        void Update()
        {
            if(CanExitState(_cursorProvider.CursorPosition, _ballAI.Position, _ballAI.BallAIData.AdditionalAvoidingDistance, _cursorProvider.CursorSize))
            {
                _stateMachine.SetActiveState<MovingToTargetState>();
                return;
            }

            CalculateVelocityDirection(_cursorProvider.CursorPosition, _ballAI.Position, out var velocityDirection);
            _physicsObject.CurrentAcceleration = velocityDirection * _ballAI.BallAIData.CurrentAcceleration;
        }

        public async UniTask OnExit()
        {
            _ballAI.OnUpdate -= Update;
        }
        
        public static void CalculateVelocityDirection(Vector2 cursorPosition, Vector2 ballPosition, out Vector2 direction)
        {
            direction = (ballPosition-cursorPosition).normalized;
        }

        public static bool CanExitState(Vector2 cursorPosition, Vector2 ballPosition, float additionalDistanceOffset, float cursorSize)
        {
            return !AIUtils.InCursorAvoidingRange(cursorPosition, ballPosition, additionalDistanceOffset, cursorSize);
        }
    }
}