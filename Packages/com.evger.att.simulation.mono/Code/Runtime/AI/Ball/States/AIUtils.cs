using UnityEngine;

namespace ATT.Simulation.Mono.AI.Ball.States
{
    public static class AIUtils
    {
        public static bool InCursorAvoidingRange(Vector2 cursorPosition, Vector2 ballPosition, float additionalDistanceOffset, float cursorSize)
        {
            return (cursorPosition - ballPosition).magnitude < additionalDistanceOffset + cursorSize;
        }
    }
}