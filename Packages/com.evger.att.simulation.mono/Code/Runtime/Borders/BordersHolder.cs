using ATT.Simulation.UnityComponents;

namespace ATT.Simulation.Mono.Borders
{
    public class BordersHolder : IBordersProvider
    {
        private BorderView _finishBorder;
        private BorderView _startBorder;

        public BorderView FinishBorder => _finishBorder;
        public BorderView StartBorder => _startBorder;

        public BordersHolder(BorderView finishBorder, BorderView startBorder)
        {
            _finishBorder = finishBorder;
            _startBorder = startBorder;
        }
    }
}