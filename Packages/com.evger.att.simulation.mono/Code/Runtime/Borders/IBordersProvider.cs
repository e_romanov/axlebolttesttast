using ATT.Simulation.UnityComponents;

namespace ATT.Simulation.Mono.Borders
{
    public interface IBordersProvider
    {
        public BorderView FinishBorder { get; }
        public BorderView StartBorder { get; }
    }
}