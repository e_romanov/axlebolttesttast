using ATT.Simulation.Mono.Physics.WorldBorder;
using UnityEngine;

namespace ATT.Simulation.Mono.Physics
{
    // updates position and speed of an object by defined physics rules 
    public class PhysicsObject : MonoBehaviour
    {
        [SerializeField] private PhysicsData _physicsData;
        [SerializeField] private float _worldBorderBumpModifier = 0.25f;
        private Transform _transform;
        private IPhysicsBordersProvider _physicsBordersProvider = new InfinitePhysicsBordersProvider();
        
        public Vector2 CurrentSpeed
        {
            get => _physicsData.CurrentSpeed;
            set => _physicsData.CurrentSpeed = value;
        }

        public Vector2 CurrentAcceleration
        {
            get => _physicsData.CurrentAcceleration;
            set => _physicsData.CurrentAcceleration = value;
        }

        public PhysicsObject Setup(IPhysicsBordersProvider physicsBordersProvider)
        {
            _physicsBordersProvider = physicsBordersProvider;
            _physicsData = new PhysicsData(Vector2.zero, Vector2.zero);
            return this;
        }

        private void Awake()
        {
            _transform = this.transform;
            if (_physicsBordersProvider is null)
            {
                // if no physics borders specified => use infinite borders
                _physicsBordersProvider = new InfinitePhysicsBordersProvider();
            }
        }

        private void Update()
        {
            float deltaTime = Time.deltaTime;
            _physicsData.CurrentSpeed = CalculateNewSpeed(_physicsData, deltaTime);
            _transform.position = CalculateNewPosition(_physicsData.CurrentSpeed, _transform.position, deltaTime);
            FitIntoWorldBorders(_transform, _physicsData, _physicsBordersProvider.BorderMin, _physicsBordersProvider.BorderMax, _worldBorderBumpModifier);
        }

        public static Vector2 CalculateNewSpeed(PhysicsData physicsData, float deltaTime)
        {
            return physicsData.CurrentSpeed += physicsData.CurrentAcceleration * deltaTime;
        }

        public static Vector3 CalculateNewPosition(Vector2 currentSpeed, Vector3 currentPosition, float deltaTime)
        {
            // as far as we deal with 2d physics, Y value ignored
            return new Vector3(currentPosition.x + currentSpeed.x * deltaTime, currentPosition.y,
                currentPosition.z + currentSpeed.y * deltaTime);
        }

        // modifies physics data and transform if object outside of world borders
        public static void FitIntoWorldBorders(Transform transform, PhysicsData physicsData, Vector2 borderMin, Vector2 borderMax, float borderBumpModifier)
        {
            Vector3 position = transform.position;
            if (position.x > borderMax.x)
            {
                position.x = borderMax.x;
                physicsData.CurrentSpeed.x *= -borderBumpModifier;
            }

            if (position.z > borderMax.y)
            {
                position.z = borderMax.y;
                physicsData.CurrentSpeed.y *= -borderBumpModifier;
            }

            if (position.x < borderMin.x)
            {
                position.x =  borderMin.x;
                physicsData.CurrentSpeed.x *= -borderBumpModifier;
            }

            if (position.z < borderMin.y)
            {
                position.z =  borderMin.y;
                physicsData.CurrentSpeed.y *= -borderBumpModifier;
            }

            transform.position = position;
        }
    }
}