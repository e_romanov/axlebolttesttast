using System;
using UnityEngine;

namespace ATT.Simulation.Mono.Physics.WorldBorder
{
    public class WorldBorderHelper : MonoBehaviour
    {
        private TransformBasedPhysicsBordersProvider _borderProvider;

        public WorldBorderHelper Setup(TransformBasedPhysicsBordersProvider borderProvider)
        {
            _borderProvider = borderProvider;
            return this;
        }

        private void OnEnable()
        {
            _borderProvider?.AddBorder(this.transform);
        }

        private void OnDisable()
        {
            _borderProvider?.RemoveBorder(this.transform);
        }
    }
}