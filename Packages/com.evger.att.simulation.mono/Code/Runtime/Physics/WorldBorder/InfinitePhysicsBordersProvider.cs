using UnityEngine;

namespace ATT.Simulation.Mono.Physics.WorldBorder
{
    public class InfinitePhysicsBordersProvider : IPhysicsBordersProvider
    {
        public Vector2 BorderMin => Vector2.negativeInfinity;
        public Vector2 BorderMax => Vector2.positiveInfinity;

        public void UpdateBorders()
        {
            return; // no need to update in this case
        }
    }
}