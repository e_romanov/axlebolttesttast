using UnityEngine;

namespace ATT.Simulation.Mono.Physics.WorldBorder
{
    public interface IPhysicsBordersProvider
    {
        public Vector2 BorderMin { get; }
        public Vector2 BorderMax { get; }

        void UpdateBorders();
    }
}