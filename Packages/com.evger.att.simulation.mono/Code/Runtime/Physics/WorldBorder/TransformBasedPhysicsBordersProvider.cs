using System.Collections.Generic;
using UnityEngine;

namespace ATT.Simulation.Mono.Physics.WorldBorder
{
    public class TransformBasedPhysicsBordersProvider : IPhysicsBordersProvider
    {
        private Vector2 _borderMin; // bottom left corner of bordering rectangle
        private Vector2 _borderMax; // top right corner of bordering rectangle

        public Vector2 BorderMin => _borderMin;
        public Vector2 BorderMax => _borderMax;

        private readonly List<Transform> _borderTransforms;

        public TransformBasedPhysicsBordersProvider()
        {
            _borderTransforms = new List<Transform>();
            _borderMin = Vector2.negativeInfinity;
            _borderMax = Vector2.positiveInfinity;
        }

        public TransformBasedPhysicsBordersProvider AddBorder(Transform transform)
        {
            if (_borderTransforms.Contains(transform))
                return this;
            _borderTransforms.Add(transform);
            transform.gameObject.AddComponent<WorldBorderHelper>().Setup(this);
            return this;
        }

        public void RemoveBorder(Transform transform)
        {
            if(_borderTransforms.Contains(transform))
                _borderTransforms.Remove(transform);
        }

        public void UpdateBorders()
        {
            _borderMin = Vector2.positiveInfinity;
            _borderMax = Vector2.negativeInfinity;

            if (_borderTransforms.Count <= 1)
                return; // only one border transform, no border rect can be found => no borders

            foreach (var transform in _borderTransforms)
            {
                if (transform.position.x < _borderMin.x)
                    _borderMin.x = transform.position.x;
                if (transform.position.z < _borderMin.y)
                    _borderMin.y = transform.position.z;

                if (transform.position.x > _borderMax.x)
                    _borderMax.x = transform.position.x;
                if (transform.position.z > _borderMax.y)
                    _borderMax.y = transform.position.z;
            }
        }
    }
}