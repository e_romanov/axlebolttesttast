using UnityEngine;

namespace ATT.Simulation.Mono.Physics
{
    [System.Serializable]
    public class PhysicsData
    {
        [SerializeField] public Vector2 CurrentSpeed;
        [SerializeField] public Vector2 CurrentAcceleration;

        public PhysicsData(Vector2 currentSpeed, Vector2 currentAcceleration)
        {
            CurrentSpeed = currentSpeed;
            CurrentAcceleration = currentAcceleration;
        }
    }
}