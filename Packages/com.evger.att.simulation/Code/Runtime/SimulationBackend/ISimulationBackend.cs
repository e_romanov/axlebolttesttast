using System;

namespace ATT.Simulation.SimulationBackend
{
    public interface ISimulationBackend : IDisposable
    {
        void StartSimulation();
        void FrameUpdate(float deltaTime);
    }
}