namespace ATT.Simulation.SimulationBackend
{
    // configures required for simulation objects and creates simulation backend
    public interface ISimulationBackendFactory
    {
        ISimulationBackend CreateBackend();
    }
}