using System;
using UnityEngine;

namespace ATT.Simulation.SimulationBackend
{
    public class SimulationRunner : MonoBehaviour
    {
        private ISimulationBackend _simulationBackend;

        public SimulationRunner Setup(ISimulationBackend simulationBackend)
        {
            _simulationBackend = simulationBackend;
            
            return this;
        }
        
        public void Update()
        {
            _simulationBackend?.FrameUpdate(Time.deltaTime);
        }

        private void OnDestroy()
        {
            _simulationBackend?.Dispose();
        }
    }
}