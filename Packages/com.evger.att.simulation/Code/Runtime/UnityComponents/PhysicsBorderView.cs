using UnityEngine;

namespace ATT.Simulation.UnityComponents
{
    public class PhysicsBorderView : MonoBehaviour
    {
        // empty for now and used simply as identifier
        // in case of specific view logic requirement as some indication once ball hit border:
        // make this class abstract, define required view methods and extend it with concrete implementation
    }
}