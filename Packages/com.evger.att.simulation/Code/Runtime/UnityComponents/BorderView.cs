using UnityEngine;

namespace ATT.Simulation.UnityComponents
{
    public enum BorderType
    {
        Finish,
        Start
    }
    
    public class BorderView : MonoBehaviour
    {
        public BorderType BorderType;

        public Vector2 Position => new Vector2(transform.position.x, transform.position.z); // transform may be cached for better performance
    }
}