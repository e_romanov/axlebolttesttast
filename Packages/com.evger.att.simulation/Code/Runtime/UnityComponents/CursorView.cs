using UnityEngine;

namespace ATT.Simulation.UnityComponents
{
    public class CursorView : MonoBehaviour
    {
        // empty for now and used simply as identifier
        // in case of specific view logic requirement f.e cursor color change once it touches ball:
        // make this class abstract, define required view methods and extend it with concrete implementation
    }
}