using UnityEngine;

namespace ATT.Simulation.UnityComponents
{
    public class BallView : MonoBehaviour
    {
        // empty for now and used simply as identifier
        // in case of specific view logic requirement f.e ball color changes, shakes on hits:
        // make this class abstract, define required view methods and extend it with concrete implementation
    }
}