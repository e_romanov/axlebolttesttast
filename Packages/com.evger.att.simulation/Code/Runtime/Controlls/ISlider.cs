namespace ATT.Simulation.Controlls
{
    public interface ISlider
    {
        public float NormalizedValue { get; set;}
    }
}