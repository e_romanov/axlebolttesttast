using UnityEngine;

namespace ATT.Simulation.Controlls
{
    // all required controls for simulation 
    [System.Serializable]
    public class ControllsGroup
    {
        private ICursor _cursor;
        private ISlider _cursorSizeSlider;
        private ISlider _accelerationSlider;
        private ISlider _velocitySlider;

        public ICursor Cursor => _cursor;
        public ISlider CursorSizeSlider => _cursorSizeSlider;
        public ISlider AccelerationSlider => _accelerationSlider;
        public ISlider VelocitySlider => _velocitySlider;

        public ControllsGroup(ICursor cursor, ISlider cursorSizeSlider, ISlider accelerationSlider, ISlider velocitySlider)
        {
            _cursor = cursor;
            _cursorSizeSlider = cursorSizeSlider;
            _accelerationSlider = accelerationSlider;
            _velocitySlider = velocitySlider;
        }
    }
}