using ATT.Simulation.Controlls;
using UnityEngine;

namespace ATT.Controls
{
    // basic implementation of Unity based ICursor
    // Input.mousePosition works on both Mobile and PC
    // not supported on consoles, so custom ICursor required for consoles
    public class UnityCursor : ICursor
    {
        public Vector3 ScreenPosition => Input.mousePosition;
    }
}