using UnityEngine;

namespace ATT.Simulation.Controlls
{
    public interface ICursor
    {
        Vector3 ScreenPosition { get; }
    }
}