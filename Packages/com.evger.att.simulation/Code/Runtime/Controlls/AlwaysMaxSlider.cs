using ATT.Simulation.Controlls;

namespace ATT.Simulation.Code.Runtime.Controlls
{
    public class AlwaysMaxSlider : ISlider
    {
        public float NormalizedValue { get => 1.0f; set{} }
    }
}