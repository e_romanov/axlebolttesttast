using ATT.Simulation.Data;
using UnityEngine;

namespace ATT.Simulation.Configuration
{
    // ReSharper disable once InconsistentNaming
    [CreateAssetMenu(menuName = "ATT/Simulation/" + nameof(EnvironmentConfigSO))]
    public class EnvironmentConfigSO : ConfigWrapperSO<EnvironmentConfig> { }
}