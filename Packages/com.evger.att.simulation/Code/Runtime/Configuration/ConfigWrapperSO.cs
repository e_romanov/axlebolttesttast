using UnityEngine;

namespace ATT.Simulation.Configuration
{
    public class ConfigWrapperSO<T> : ScriptableObject
    {
        [SerializeField] private T _serializedInstance;
        
        public virtual T Config => _serializedInstance;
    }
}