using UnityEngine;

namespace ATT.Simulation.Configuration
{
    [System.Serializable]
    public class BallConfig
    {
        [SerializeField] private float _minAcceleration;
        [SerializeField] private float _maxAcceleration;
        [SerializeField] private float _minTargetVelocity;
        [SerializeField] private float _maxTargetVelocity;

        public float MinAcceleration => _minAcceleration;
        public float MaxAcceleration => _maxAcceleration;

        public float MinTargetVelocity => _minTargetVelocity;
        public float MaxTargetVelocity => _maxTargetVelocity;
    }
}