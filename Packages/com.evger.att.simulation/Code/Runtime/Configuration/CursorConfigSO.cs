using UnityEngine;

namespace ATT.Simulation.Configuration
{
    [CreateAssetMenu(menuName = "ATT/Simulation" + nameof(CursorConfigSO))]
    public class CursorConfigSO : ConfigWrapperSO<CursorConfig> { }
}