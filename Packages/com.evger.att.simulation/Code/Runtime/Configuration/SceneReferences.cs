using ATT.Simulation.UnityComponents;
using UnityEngine;

namespace ATT.Simulation.Configuration
{
    [System.Serializable]
    public class SceneReferences
    {
        [SerializeField] private BorderView _finishBorder;
        [SerializeField] private BorderView _startBorder;
        [Tooltip("Requires at least 2 border objects")]
        [SerializeField] private PhysicsBorderView[] _physicsBorders;

        public BorderView FinishBorder => _finishBorder;
        public BorderView StartBorder => _startBorder;
        public PhysicsBorderView[] PhysicsBorders => _physicsBorders;

        public SceneReferences(BorderView finishBorder, BorderView startBorder, PhysicsBorderView[] physicsBorders)
        {
            _finishBorder = finishBorder;
            _startBorder = startBorder;
            _physicsBorders = physicsBorders;
        }
    }
}