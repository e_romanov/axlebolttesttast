using ATT.Simulation.UnityComponents;
using UnityEngine;

namespace ATT.Simulation.Data
{
    [System.Serializable]
    public class EnvironmentConfig
    {
        [Header("Views")]
        [SerializeField] private BallView _ballViewPrefab;
        [SerializeField] private CursorView _cursorView;
        [Header("Cursor raycasts")]
        [SerializeField] private LayerMask _cursorDisplacementRaycastMask;
        [SerializeField] private float _maxRaycastDistance;

        [Header("Additional")] 
        [Tooltip("If ball farther than safe distance + distance to cursor, it will go to finish")]
        [SerializeField] private float _ballSafeDistance;
        [Tooltip("By how much speed will be modified on world borders bump"), Range(0.0f, 1.0f)]
        [SerializeField] private float _worldBordersBumpSpeedModifier = 0.25f;

        public BallView BallViewPrefab => _ballViewPrefab;
        public CursorView CursorView => _cursorView;
        public int CursorDisplacementRaycastMask => _cursorDisplacementRaycastMask.value;
        public float MaxRaycastDistance => _maxRaycastDistance;
        public float BallSafeDistance => _ballSafeDistance;
        public float WorldBordersBumpSpeedModifier => _worldBordersBumpSpeedModifier;

        public EnvironmentConfig(BallView ballViewPrefab, CursorView cursorView, LayerMask cursorDisplacementRaycastMask, float maxRaycastDistance, float ballSafeDistance, float worldBordersBumpSpeedModifier)
        {
            _ballViewPrefab = ballViewPrefab;
            _cursorView = cursorView;
            _cursorDisplacementRaycastMask = cursorDisplacementRaycastMask;
            _maxRaycastDistance = maxRaycastDistance;
            _ballSafeDistance = ballSafeDistance;
            _worldBordersBumpSpeedModifier = worldBordersBumpSpeedModifier;
        }
    }
}