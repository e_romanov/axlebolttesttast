using UnityEngine;

namespace ATT.Simulation.Configuration
{
    [System.Serializable]
    public class CursorConfig
    {
        [SerializeField] private float _minScale;
        [SerializeField] private float _maxScale;

        public float MinScale => _minScale;
        public float MaxScale => _maxScale;

        public CursorConfig(float minScale, float maxScale)
        {
            _minScale = minScale;
            _maxScale = maxScale;
        }
    }
}