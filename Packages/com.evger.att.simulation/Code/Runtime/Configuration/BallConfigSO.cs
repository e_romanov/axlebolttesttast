using UnityEngine;

namespace ATT.Simulation.Configuration
{
    // ReSharper disable once InconsistentNaming
    [CreateAssetMenu(menuName = "ATT/Simulation" + nameof(BallConfigSO))]
    public class BallConfigSO : ConfigWrapperSO<BallConfig> {}
}