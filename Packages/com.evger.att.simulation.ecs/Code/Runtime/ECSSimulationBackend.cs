using ATT.Simulation.SimulationBackend;
using ATT.Simulation.ECS.Signals;
using Leopotam.Ecs;

namespace ATT.Simulation.ECS
{
    public class ECSSimulationBackend : ISimulationBackend
    {
        private EcsWorld _world;
        private EcsSystems _systems;
        private DeltaTime _deltaTime;

        public ECSSimulationBackend(EcsWorld world, EcsSystems systems, DeltaTime deltaTime)
        {
            _world = world;
            _systems = systems;
            _deltaTime = deltaTime;
        }
        
        public void StartSimulation()
        {
            _world.NewEntity().Get<StartSimulationSignal>();
        }

        public void FrameUpdate(float deltaTime)
        {
            _deltaTime.Value = deltaTime;
            _systems?.Run ();
        }

        public void Dispose () {
            if (_systems != null) {
                _systems.Destroy ();
                _systems = null;
                _world.Destroy ();
                _world = null;
            }
        }
    }
}