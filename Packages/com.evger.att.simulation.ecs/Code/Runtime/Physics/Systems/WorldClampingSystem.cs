using ATT.Simulation.ECS.Transforms.Components;
using ATT.Simulation.ECS.Physics.Components;
using Leopotam.Ecs;
using UnityEngine;

namespace ATT.Simulation.ECS.Physics.Systems
{
    // provides collision with world borders, keeps physics objects inside physics world borders
    public class WorldClampingSystem : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<PhysicsWorldBordersComponent> _borders;
        private readonly EcsFilter<PhysicsObject, TransformComponent> _targets;

        private float _worldBordersBumpSpeedModifier;

        public WorldClampingSystem(float worldBordersBumpSpeedModifier)
        {
            _worldBordersBumpSpeedModifier = worldBordersBumpSpeedModifier;
        }

        public WorldClampingSystem()
        {
            _worldBordersBumpSpeedModifier = 0.25f;
        }

        public void Run()
        {
            // by design there is only one PhysicsWorldBordersComponent
            // if there is more such components, objects will fit in both
            foreach (var borderIdx in _borders)
            {
                ref var worldBorders =ref _borders.Get1(borderIdx);
                ClampTargetsIntoBorders( ref worldBorders.WorldMin, ref worldBorders.WorldMax, _targets);
            }
        }

        private void ClampTargetsIntoBorders(ref Vector2 worldMin, ref Vector2 worldMax, EcsFilter<PhysicsObject, TransformComponent> targets)
        {
            foreach (var targetIdx in targets)
            {
                ref var phyicsBody = ref targets.Get1(targetIdx);
                ref var transform = ref targets.Get2(targetIdx);
                if (transform.Position.x > worldMax.x)
                {
                    transform.Position.x = worldMax.x;
                    phyicsBody.CurrentSpeed.x *= -_worldBordersBumpSpeedModifier;
                }

                if (transform.Position.y > worldMax.y)
                {
                    transform.Position.y = worldMax.y;
                    phyicsBody.CurrentSpeed.y *= -_worldBordersBumpSpeedModifier;
                }

                if (transform.Position.x < worldMin.x)
                {
                    transform.Position.x = worldMin.x;
                    phyicsBody.CurrentSpeed.x *= -_worldBordersBumpSpeedModifier;
                }

                if (transform.Position.y < worldMin.y)
                {
                    transform.Position.y = worldMin.y;
                    phyicsBody.CurrentSpeed.y *= -_worldBordersBumpSpeedModifier;
                }
            }
        }
    }
}