using ATT.Simulation.ECS.Physics.Components;
using Leopotam.Ecs;

namespace ATT.Simulation.ECS.Physics.Systems
{
    // modifies speed by acceleration
    public class AccelerationSystem : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<PhysicsObject> _targets;
        private readonly DeltaTime _deltaTime;

        public void Run()
        {
            foreach (var targetIdx in _targets)
            {
                _targets.Get1(targetIdx).CurrentSpeed += _targets.Get1(targetIdx).CurrentAcceleration * _deltaTime.Value;
            }
        }
    }
}