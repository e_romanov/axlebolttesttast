using ATT.Simulation.ECS.Transforms.Components;
using ATT.Simulation.ECS.Physics.Components;
using Leopotam.Ecs;

namespace ATT.Simulation.ECS.Physics.Systems
{
    // modifies position by speed
    public class DisplacementSystem : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<PhysicsObject, TransformComponent> _targets;
        private readonly DeltaTime _deltaTime;

        public void Run()
        {
            foreach (var targetIdx in _targets)
            {
                _targets.Get2(targetIdx).Position += _targets.Get1(targetIdx).CurrentSpeed * _deltaTime.Value;
            }
        }
    }
}