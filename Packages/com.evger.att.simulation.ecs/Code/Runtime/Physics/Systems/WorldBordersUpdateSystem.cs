using ATT.Simulation.ECS.Transforms.Components;
using ATT.Simulation.ECS.Physics.Components;
using Leopotam.Ecs;
using UnityEngine;

namespace ATT.Simulation.ECS.Physics.Systems
{
    public class WorldBordersUpdateSystem<BorderTagT> : IEcsRunSystem where BorderTagT : struct
    {
        // auto injected fields
        private readonly EcsFilter<BorderTagT, TransformComponent> _targets;
        private readonly EcsFilter<PhysicsWorldBordersComponent> _borders;
        private readonly EcsWorld _world;

        public void Run()
        {
            CalculateWorldBorders(out var worldMin, out var worldMax, _targets);
            UpdateBordersComponent(ref worldMin, ref worldMax, _borders);
        }

        private void CalculateWorldBorders(out Vector2 worldMin, out Vector2 worldMax, EcsFilter<BorderTagT, TransformComponent>  targets)
        {
            worldMin = Vector2.positiveInfinity;
            worldMax = Vector2.negativeInfinity;
            
            foreach (var borderIdx in targets)
            {
                ref var transform = ref targets.Get2(borderIdx);

                if (transform.Position.x < worldMin.x)
                    worldMin.x = transform.Position.x;
                if (transform.Position.y < worldMin.y)
                    worldMin.y = transform.Position.y;

                if (transform.Position.x > worldMax.x)
                    worldMax.x = transform.Position.x;
                if (transform.Position.y > worldMax.y)
                    worldMax.y = transform.Position.y;
            }
        }

        private void UpdateBordersComponent(ref Vector2 worldMin, ref Vector2 worldMax, EcsFilter<PhysicsWorldBordersComponent> borders)
        {
            // ensure that there is always at least one world border
            if (_borders.IsEmpty())
                _world.NewEntity().Get<PhysicsWorldBordersComponent>();

            foreach (var borderIdx in borders)
            {
                ref var border = ref borders.Get1(borderIdx);
                border.WorldMax = worldMax;
                border.WorldMin = worldMin;
            }
        }
    }
}