using UnityEngine;

namespace ATT.Simulation.ECS.Physics.Components
{
    public struct PhysicsObject
    {
        public Vector2 CurrentSpeed;
        public Vector2 CurrentAcceleration;
    }
}