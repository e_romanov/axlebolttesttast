using UnityEngine;

namespace ATT.Simulation.ECS.Physics.Components
{
    public struct PhysicsWorldBordersComponent
    {
        // bottom left physics world corner
        public Vector2 WorldMin;
        
        // top right physics world corner
        public Vector2 WorldMax;
    }
}