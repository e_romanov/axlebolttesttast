using UnityEngine;

namespace ATT.Simulation.ECS.Transforms.Components
{
    public struct UnityTransform
    {
        public Transform Transform;
    }
}