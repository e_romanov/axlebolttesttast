using UnityEngine;

namespace ATT.Simulation.ECS.Transforms.Components
{
    public struct TransformComponent
    {
        public Vector2 Position;
        public Vector3 Scale;
    }
}