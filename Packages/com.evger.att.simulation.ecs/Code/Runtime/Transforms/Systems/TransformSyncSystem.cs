using ATT.Simulation.ECS.Transforms.Components;
using Leopotam.Ecs;
using UnityEngine;

namespace ATT.Simulation.ECS.Transforms.Systems
{
    public class TransformSyncSystem : IEcsRunSystem
    {
        private readonly EcsFilter<SyncTransform, TransformComponent, UnityTransform> _targets;

        public void Run()
        {
            foreach (var targetIdx in _targets)
            {
                Transform unityTransform =  _targets.Get3(targetIdx).Transform;
                ref var ecsTransform = ref _targets.Get2(targetIdx);
                
                if (_targets.Get1(targetIdx).WriteToUnity)
                {
                    // ecs -> unity
                    unityTransform.position = new Vector3(ecsTransform.Position.x,unityTransform.position.y,ecsTransform.Position.y);
                    unityTransform.localScale = ecsTransform.Scale;
                }
                else
                {
                    // unity -> ecs
                    ecsTransform.Position = new Vector2(unityTransform.position.x, unityTransform.position.z);
                    ecsTransform.Scale = unityTransform.localScale;
                }
                    
            }
        }
    }
}