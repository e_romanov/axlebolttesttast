using Leopotam.Ecs;

namespace ATT.Simulation.ECS.Signals
{
    // one-frame signal to report ecs systems about simulation start(when ball should start moving)
    public struct StartSimulationSignal : IEcsIgnoreInFilter
    {
        
    }
}