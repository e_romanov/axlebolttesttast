using ATT.Simulation.ECS.Signals;
using Leopotam.Ecs;

namespace ATT.Simulation.ECS.Ball.Systems
{
    // ball initialized without any of state components before receiving StartSimulationSignal
    // once ball has state component, it begin to act and StateUpdateSystem will change its state if needed
    public class StartSimulationProcessing<FirstStateOfBallT> : IEcsRunSystem where FirstStateOfBallT : struct
    {
        private readonly EcsFilter<StartSimulationSignal> _signal;
        private readonly EcsFilter<Components.Ball> _ball;

        public void Run()
        {
            if(_signal.IsEmpty())
                return;

            foreach (var ballIdx in _ball)
            {
                _ball.GetEntity(ballIdx).Get<FirstStateOfBallT>();
            }
        }
    }
}