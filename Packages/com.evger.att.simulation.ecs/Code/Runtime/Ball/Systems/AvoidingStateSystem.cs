using ATT.Simulation.ECS.Ball.Components;
using ATT.Simulation.ECS.Transforms.Components;
using ATT.Simulation.ECS.Cursor.Components;
using ATT.Simulation.ECS.Physics.Components;
using Leopotam.Ecs;
using UnityEngine;

namespace ATT.Simulation.ECS.Ball.Systems
{
    // ball ai during avoiding state
    public class AvoidingStateSystem : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<AvoidingState, Components.Ball, TransformComponent, PhysicsObject> _balls;
        private readonly EcsFilter<CursorComponent, TransformComponent> _cursors;
        private readonly EcsFilter<PhysicsWorldBordersComponent> _worldBorder;

        public void Run()
        {
           
            foreach (var ballIdx in _balls)
            {
                ref var ballTransform = ref _balls.Get3(ballIdx);
                FindClosestCursorPosition(_cursors, out var resultingPosition, ref ballTransform.Position);
                CalculateVelocityDirection(ref resultingPosition, ref ballTransform.Position, out var resultingDirection);
                _balls.Get4(ballIdx).CurrentAcceleration = resultingDirection * _balls.Get2(ballIdx).CurrentAcceleration;
            }
        }

        private void FindClosestCursorPosition(EcsFilter<CursorComponent, TransformComponent> cursors, out Vector2 resultingPosition, ref Vector2 ballPosition)
        {
            float resultingDistanceSquared = Mathf.Infinity;
            resultingPosition = Vector2.positiveInfinity;
            
            foreach (var cursorIdx in cursors)
            {
                float distanceSquared = (_cursors.Get2(cursorIdx).Position - ballPosition).sqrMagnitude;
                
                if (distanceSquared < resultingDistanceSquared)
                {
                    resultingDistanceSquared = distanceSquared;
                    resultingPosition = _cursors.Get2(cursorIdx).Position;
                }
            }
        }

        private void CalculateVelocityDirection(ref Vector2 cursorPosition, ref Vector2 ballPosition, out Vector2 direction)
        {
            direction = (ballPosition-cursorPosition).normalized;
        }
    }
}