using ATT.Simulation.Controlls;
using Leopotam.Ecs;
using UnityEngine;

namespace ATT.Simulation.ECS.Ball.Systems
{
    // syncs target acceleration of a ball with provided ISlider
    public class BallTargetAccelerationUpdateSystem : IEcsRunSystem
    {
        // auto injected field
        private readonly EcsFilter<Components.Ball> _balls;
        
        private readonly ISlider _slider;

        public BallTargetAccelerationUpdateSystem(ISlider slider)
        {
            _slider = slider;
        }

        public void Run()
        {
            foreach (var ballIdx in _balls)
            {
                ref var ball = ref _balls.Get1(ballIdx);
                ball.CurrentAcceleration = Mathf.Lerp(ball.MinAcceleration, ball.MaxAcceleration, _slider.NormalizedValue);
            }
        }
    }
}