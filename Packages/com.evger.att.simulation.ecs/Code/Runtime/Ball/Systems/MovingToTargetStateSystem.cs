using ATT.Simulation.ECS.Ball.Components;
using ATT.Simulation.ECS.Borders.Components;
using ATT.Simulation.ECS.Transforms.Components;
using ATT.Simulation.ECS.Physics.Components;
using Leopotam.Ecs;
using UnityEngine;

namespace ATT.Simulation.ECS.Ball.Systems
{
    // ball ai during moving to target state
    // based on direction to finish border modifies physics acceleration of a ball
    public class MovingToTargetStateSystem : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<MovingToTargetState, Components.Ball, TransformComponent, PhysicsObject> _ball;
        private readonly EcsFilter<FinishBorderTag, TransformComponent> _finishBorder;
        private DeltaTime _deltaTime;

        public void Run()
        {
            foreach (var ballIdx in _ball)
            {
                ref var ball = ref _ball.Get2(ballIdx);
                ref var ballTransform = ref _ball.Get3(ballIdx);
                ref var ballPhysics = ref _ball.Get4(ballIdx);
                Vector2 borderPosition = FindClosestBorderPosition(_finishBorder, ref ballTransform.Position);
                ballPhysics.CurrentAcceleration = CalculateAcceleration(ref ballTransform.Position, ref borderPosition, ref ballPhysics, ref ball);
            }
            
        }

        private Vector2 FindClosestBorderPosition(EcsFilter<FinishBorderTag, TransformComponent> borders, ref Vector2 ballPosition)
        {
            Vector2 result = Vector2.positiveInfinity;
            float resultingDistance = Mathf.Infinity;
            foreach (var borderIdx in borders)
            {
                ref var borderTransform = ref borders.Get2(borderIdx);
                float newDistance = Vector2.SqrMagnitude(borderTransform.Position-ballPosition);
                if (newDistance < resultingDistance)
                {
                    resultingDistance = newDistance;
                    result = borderTransform.Position;
                }
            }
            
            return result;
        }

        private Vector2 CalculateAcceleration(ref Vector2 ballPosition, ref Vector2 borderPosition, ref PhysicsObject ballPhysics, ref Components.Ball ball)
        {
            Vector2 direction = (borderPosition - ballPosition);
            // if close enough - compensate speed, better to remove magical numbers in future
            direction = direction.sqrMagnitude < 0.35f ? -ballPhysics.CurrentSpeed.normalized : direction.normalized;
            
            Vector2 speedDelta =  (direction * ball.TargetSpeed) - ballPhysics.CurrentSpeed;
            
            if(speedDelta.sqrMagnitude < 0.5f)
                return Vector2.zero;

            if (speedDelta.magnitude * _deltaTime.Value < ball.CurrentAcceleration * _deltaTime.Value) 
                return speedDelta;

            return speedDelta.normalized * ball.CurrentAcceleration;
        }
    }
}