using ATT.Simulation.ECS.Ball.Components;
using ATT.Simulation.ECS.Transforms.Components;
using ATT.Simulation.ECS.Cursor.Components;
using Leopotam.Ecs;
using UnityEngine;

namespace ATT.Simulation.ECS.Ball.Systems
{
    // Transits to AvoidingState when ball is in range of at least one cursor
    // Transits to MovingToTargetState when ball is not in range of at least one cursor
    public class BallStateUpdateSystem: IEcsRunSystem 
    {
        // auto injected fields
        private readonly EcsFilter<Components.Ball, TransformComponent> _targets;
        private readonly EcsFilter<CursorComponent, TransformComponent> _cursors;
        
        private float _ballSafeDistance;

        public BallStateUpdateSystem(float ballSafeDistance)
        {
            _ballSafeDistance = ballSafeDistance;
        }

        public BallStateUpdateSystem()
        {
            _ballSafeDistance = 2.5f;
        }

        public void Run()
        {
            foreach (var targetIdx in _targets)
            {
                ref var ballTransform = ref _targets.Get2(targetIdx);

                bool cursorInRangeFound = false;
                foreach (var cursorIdx in _cursors)
                {
                    ref var cursorTransform = ref _cursors.Get2(cursorIdx);
                    ref var cursor = ref _cursors.Get1(cursorIdx);

                    if (CursorInRange(ref cursorTransform.Position, ref cursor.Size, ref ballTransform.Position,
                        ref _ballSafeDistance))
                    {
                        cursorInRangeFound = true;
                        break; // this target has at least one cursor in avoiding range
                    }
                }

                if (cursorInRangeFound && _targets.GetEntity(targetIdx).Has<MovingToTargetState>())
                {
                    _targets.GetEntity(targetIdx).Del<MovingToTargetState>();
                    _targets.GetEntity(targetIdx).Get<AvoidingState>();
                }
                else if(!cursorInRangeFound && _targets.GetEntity(targetIdx).Has<AvoidingState>())
                {
                    _targets.GetEntity(targetIdx).Del<AvoidingState>();
                    _targets.GetEntity(targetIdx).Get<MovingToTargetState>();
                }
            }
        }

        public static bool CursorInRange(ref Vector2 cursorPosition, ref float cursorSize, ref Vector2 ballPosition, ref float safeDistance)
        {
            return (cursorPosition - ballPosition).magnitude < (cursorSize + safeDistance);
        }
    }
}