namespace ATT.Simulation.ECS.Ball.Components
{
    public struct Ball
    {
        public float TargetSpeed;
        public float MinTargetSpeed;
        public float MaxTargetSpeed;
        
        public float CurrentAcceleration;
        public float MinAcceleration;
        public float MaxAcceleration;
    }
}