using ATT.Simulation.UnityComponents;

namespace ATT.Simulation.ECS.Borders
{
    public struct BorderViewRef
    {
        public BorderView BorderView;
    }
}