using Leopotam.Ecs;

namespace ATT.Simulation.ECS.Borders.Components
{
    // additional tag for simplified filtering
    public struct StartBorderTag : IEcsIgnoreInFilter { }
}