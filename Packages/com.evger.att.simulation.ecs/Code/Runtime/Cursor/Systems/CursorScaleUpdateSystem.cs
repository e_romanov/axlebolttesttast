using ATT.Simulation.Controlls;
using ATT.Simulation.ECS.Transforms.Components;
using Leopotam.Ecs;
using UnityEngine;

namespace ATT.Simulation.ECS.Cursor.Systems
{
    // syncs cursors size value with provided ISlider 
    public class CursorScaleUpdateSystem : IEcsRunSystem
    {
        // auto injected field
        private readonly EcsFilter<ECS.Cursor.Components.CursorComponent, TransformComponent> _cursors;
        
        private ISlider _cursorSlider;
        
        public CursorScaleUpdateSystem(ISlider cursorSlider)
        {
            _cursorSlider = cursorSlider;
        }

        public void Run()
        {
            foreach (var cursorIdx in _cursors)
            {
                ref var cursor = ref _cursors.Get1(cursorIdx);
                cursor.Size = Mathf.Lerp(cursor.MinScale, cursor.MaxScale, _cursorSlider.NormalizedValue);

                ref var cursorTransform = ref _cursors.Get2(cursorIdx);
                cursorTransform.Scale = Vector3.one * cursor.Size;
            }
        }
    }
}