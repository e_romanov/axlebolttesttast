using ATT.Simulation.Controlls;
using ATT.Simulation.ECS.Transforms.Components;
using ATT.Simulation.ECS.Cursor.Components;
using Leopotam.Ecs;
using UnityEngine;

namespace ATT.Simulation.ECS.Cursor.Systems
{
    // projects provided ICursor positions to 3d world and updates cursors transforms based on result of projections
    public class CursorDisplacementSystem : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<CursorComponent, TransformComponent> _cursors;
        private DeltaTime _deltaTime;
        
        private readonly Camera _targetCamera;
        private readonly ICursor _cursor;
        
        private readonly int _raycastMask;
        private readonly float _maxRaycastDistance;
        
        // runtime data
        private RaycastHit hitResult;
        
        public CursorDisplacementSystem(ICursor cursor, int raycastMask, float maxRaycastDistance, Camera targetCamera)
        {
            _targetCamera = targetCamera;
            _cursor = cursor;
            _raycastMask = raycastMask;
            _maxRaycastDistance = maxRaycastDistance;
        }

        public CursorDisplacementSystem(ICursor cursor, int raycastMask, float maxRaycastDistance) : this(cursor, raycastMask, maxRaycastDistance, Camera.main)
        {
        }
        
        public void Run()
        {
            if (!UnityEngine.Physics.Raycast(_targetCamera.ScreenPointToRay(_cursor.ScreenPosition),
                out hitResult, _maxRaycastDistance, _raycastMask)) return;
            
            
            foreach (var cursorIdx in _cursors)
            {
                // use lerp to do some neat smoothing, better to remove magical number 30.0f in the future
                _cursors.Get2(cursorIdx).Position.x = Mathf.Lerp(_cursors.Get2(cursorIdx).Position.x , hitResult.point.x, _deltaTime.Value*30.0f);
                _cursors.Get2(cursorIdx).Position.y = Mathf.Lerp(_cursors.Get2(cursorIdx).Position.y , hitResult.point.z, _deltaTime.Value*30.0f);
            }
        }
    }
}