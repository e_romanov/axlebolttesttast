namespace ATT.Simulation.ECS.Cursor.Components
{
    public struct CursorComponent
    {
        public float Size;
        public float MinScale;
        public float MaxScale;
    }
}