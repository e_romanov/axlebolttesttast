using ATT.Simulation.UnityComponents;

namespace ATT.Simulation.ECS.Cursor.Components
{
    public struct CursorViewRef
    {
        public CursorView CursorView;
    }
}