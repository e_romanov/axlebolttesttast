using ATT.Simulation.Configuration;
using ATT.Simulation.Controlls;
using ATT.Simulation.Data;
using ATT.Simulation.SimulationBackend;
using ATT.Simulation.UnityComponents;
using ATT.Simulation.ECS.Ball.Components;
using ATT.Simulation.ECS.Borders;
using ATT.Simulation.ECS.Borders.Components;
using ATT.Simulation.ECS.Transforms.Components;
using ATT.Simulation.ECS.Cursor.Components;
using ATT.Simulation.ECS.Physics.Components;
using ATT.Simulation.ECS.Signals;
using Leopotam.Ecs;
using UnityEngine;

namespace ATT.Simulation.ECS
{
    // configures simulation objects and provides simulation backend instance
    // in case of further extension of required initialization steps - will be better to switch to builder
    // ex
    // backendBuilder
    //      .CreateWorldBorders(some params)
    //      .AddBall(some params)
    //      .AddCursor(some params)
    //      .Build(some params)
    public class ECSSimulationBackendFactory : ISimulationBackendFactory
    {
        private EnvironmentConfig _environmentConfig;
        private BallConfig _ballConfig;
        private CursorConfig _cursorConfig;
        private ControllsGroup _controllsGroup;
        private SceneReferences _sceneReferences;

        public ECSSimulationBackendFactory(EnvironmentConfig environmentConfig, BallConfig ballConfig, CursorConfig cursorConfig, ControllsGroup controllsGroup, SceneReferences sceneReferences)
        {
            _environmentConfig = environmentConfig;
            _ballConfig = ballConfig;
            _cursorConfig = cursorConfig;
            _controllsGroup = controllsGroup;
            _sceneReferences = sceneReferences;
        }

        public ISimulationBackend CreateBackend()
        {
            EcsWorld world = new EcsWorld ();
            EcsSystems systems = new EcsSystems (world);
            DeltaTime deltaTime = new DeltaTime();
            
#if UNITY_EDITOR
            // UNITY_EDITOR check outside of the method to prevent possible code stripping issues
            // stripping may remove whole method if method will be empty in build
            // another solution relies on [Preserve] attribute, but less trusted
            CreateDebugObservers(world, systems);
#endif
            
            RegisterSystems(systems);
            systems.Inject(deltaTime);
            systems.Init();
            CreateEntities(world);
            
            return new ECSSimulationBackend(world, systems, deltaTime);
        }
        
#if UNITY_EDITOR
        private void CreateDebugObservers(EcsWorld world, EcsSystems systems)
        {
            Leopotam.Ecs.UnityIntegration.EcsWorldObserver.Create (world);
            Leopotam.Ecs.UnityIntegration.EcsSystemsObserver.Create (systems);
        }
#endif

        private void CreateEntities(EcsWorld world)
        {
            // borders for physics simulation
            CreatePhysicsBorders(_sceneReferences.PhysicsBorders, world);
            
            // setup finish border object, where ball should finish its movement
            SetupBorder(_sceneReferences.FinishBorder, world);
            // setup start border object, where ball will start its movement
            EcsEntity startBorder = SetupBorder(_sceneReferences.StartBorder, world);
            
            CreateBall(_environmentConfig.BallViewPrefab, _ballConfig,
                startBorder.Get<BorderViewRef>().BorderView.transform.position, world);
            
            CreateCursor(_environmentConfig.CursorView, _cursorConfig, world);
        }

        private void RegisterSystems(EcsSystems systems)
        {
            systems
                .Add(new Transforms.Systems.TransformSyncSystem())

                .Add(new Ball.Systems.StartSimulationProcessing<MovingToTargetState>())
                .Add(new Ball.Systems.BallTargetAccelerationUpdateSystem(_controllsGroup.AccelerationSlider))
                .Add(new Ball.Systems.BallTargetVelocityUpdateSystem(_controllsGroup.VelocitySlider))
                .Add(new Ball.Systems.BallStateUpdateSystem(_environmentConfig.BallSafeDistance))
                .Add(new Ball.Systems.AvoidingStateSystem())
                .Add(new Ball.Systems.MovingToTargetStateSystem())

                .Add(new Cursor.Systems.CursorDisplacementSystem(_controllsGroup.Cursor, _environmentConfig.CursorDisplacementRaycastMask, _environmentConfig.MaxRaycastDistance))
                .Add(new Cursor.Systems.CursorScaleUpdateSystem(_controllsGroup.CursorSizeSlider))

                .Add(new Physics.Systems.AccelerationSystem())
                .Add(new Physics.Systems.DisplacementSystem())
                .Add(new Physics.Systems.WorldBordersUpdateSystem<PhysicsBorderTag>())
                .Add(new Physics.Systems.WorldClampingSystem(_environmentConfig.WorldBordersBumpSpeedModifier))

                .OneFrame<StartSimulationSignal>();
        }

        // can be moved to separate IBallFactory for further extendability
        public static EcsEntity CreateBall(BallView ballViewPrefab, BallConfig ballConfig, Vector3 targetPosition, EcsWorld world)
        {
            BallView ballView = Object.Instantiate(ballViewPrefab);
            ballView.transform.position = targetPosition;
            EcsEntity entity = world.NewEntity();

            ref var ballComponent = ref entity.Get<Ball.Components.Ball>();
            ballComponent.MinAcceleration = ballConfig.MinAcceleration;
            ballComponent.MaxAcceleration = ballConfig.MaxAcceleration;
            ballComponent.CurrentAcceleration = ballComponent.MinAcceleration;

            ballComponent.MinTargetSpeed = ballConfig.MinTargetVelocity;
            ballComponent.MaxTargetSpeed = ballConfig.MaxTargetVelocity;
            ballComponent.TargetSpeed = ballConfig.MinTargetVelocity;

            entity.Get<PhysicsObject>();
            AttachTransformComponent(entity, ballView.transform);
            SetupSyncTransform(entity, ballView.transform);

            return entity;
        }

        // can be moved to separate IPhysicsBorderFactory for further extendability
        public static EcsEntity CreatePhysicsBorder(PhysicsBorderView view, EcsWorld world)
        {
            EcsEntity entity = world.NewEntity();
            entity.Get<PhysicsBorderTag>();
            AttachTransformComponent(entity, view.transform);
            SetupSyncTransform(entity, view.transform, false);
            return entity;
        }

        public static void CreatePhysicsBorders(PhysicsBorderView[] views, EcsWorld world)
        {
            for (int i = 0; i < views.Length; i++)
            {
                CreatePhysicsBorder(views[i], world);
            }
        }
        
        // can be moved to separate IBallFactory for further extendability
        public static EcsEntity SetupBorder(BorderView borderView, EcsWorld world)
        {
            EcsEntity entity = world.NewEntity();
            entity.Get<BorderTag>();
            entity.Get<BorderViewRef>().BorderView = borderView;
            if (borderView.BorderType == BorderType.Start)
            {
                entity.Get<StartBorderTag>();
            }
            else
            {
                entity.Get<FinishBorderTag>();
            }

            AttachTransformComponent(entity, borderView.transform);
            SetupSyncTransform(entity, borderView.transform, false);
            
            return entity;
        }

        // can be moved to separate ICursorFactory for further extendability
        public static EcsEntity CreateCursor(CursorView cursorViewPrefab, CursorConfig cursorConfig, EcsWorld world)
        {
            CursorView cursorView = Object.Instantiate(cursorViewPrefab);
            
            EcsEntity entity = world.NewEntity();
            
            entity.Get<CursorViewRef>().CursorView = cursorView;
            
            ref var cursorComponent = ref entity.Get<CursorComponent>();
            cursorComponent.Size = Mathf.Lerp(cursorConfig.MinScale, cursorConfig.MaxScale, 0.5f);
            cursorComponent.MinScale = cursorConfig.MinScale;
            cursorComponent.MaxScale = cursorConfig.MaxScale;

            AttachTransformComponent(entity, cursorView.transform);
            SetupSyncTransform(entity, cursorView.transform);
            return entity;
        }
        
        public static void AttachTransformComponent(EcsEntity target, Transform referencePosition)
        {
            target.Get<TransformComponent>().Position = new Vector2(referencePosition.position.x, referencePosition.position.z);
            target.Get<TransformComponent>().Scale = referencePosition.localScale;
        }

        public static void SetupSyncTransform(EcsEntity target, Transform targetTransform, bool syncToUnity = true )
        {
            target.Get<UnityTransform>().Transform = targetTransform;
            target.Get<SyncTransform>().WriteToUnity = syncToUnity;
        }
    }
}