namespace ATT.Simulation.ECS
{
    // used to pass delta time to ecs systems
    // better to use this instead of Time.deltaTime to support cases where standard deltaTime not available
    // ex validations when its needed to run set of systems several times outside of unity's update loop
    public class DeltaTime
    {
        public float Value;
    }
}