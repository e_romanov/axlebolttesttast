using ATT.Controls;
using ATT.Simulation.Configuration;
using ATT.Simulation.Controlls;
using ATT.Simulation.SimulationBackend;
using ATT.Simulation.ECS;
using ATT.Simulation.Mono;
using ATT.States;
using ATT.UI.Screens;
using Cysharp.Threading.Tasks;
using SimpleStateMachine;
using UnityEngine;
using ISlider = ATT.UI.Slider.ISlider;

namespace ATT
{
    public class CompositionRoot : MonoBehaviour
    {
        // enum is ok in that case and dont bring additional layer of complexity
        internal enum SimulationBackend
        {
            ECS,
            Mono
        }

        [Header("Backend")] 
        [SerializeField] private SimulationBackend _simulationBackend;

        [Header("Configs")]
        [SerializeField] private EnvironmentConfigSO _environmentConfig;
        [SerializeField] private BallConfigSO _ballConfig;
        [SerializeField] private CursorConfigSO _cursorConfig;
        
        // using UnityEngine.Object to support both MonoBehaviour and ScriptableObject references
        [Header("UI")]
        [Tooltip("Requires unity object which implements ATT.UI.Sliders.ISlider")]
        [SerializeField] private UnityEngine.Object _accelerationSlider;
        [Tooltip("Requires unity object which implements ATT.UI.Sliders.ISlider")]
        [SerializeField] private UnityEngine.Object _cursorValueSlider;
        [Tooltip("Requires unity object which implements ATT.UI.Sliders.ISlider")]
        [SerializeField] private UnityEngine.Object _velocitySlider;

        [SerializeField] private TapToStartSreen _ttsScreen;
        [SerializeField] private MonoUIScreen _simulationScreen;

        [Header("Scene references")] 
        [SerializeField] private SceneReferences _sceneReferences;

        public void Awake()
        {
            ControllsGroup controllsGroup = SetupControlls(_accelerationSlider, _cursorValueSlider, _velocitySlider);
            ISimulationBackend simulationBackend = CreateSimulationBackend(controllsGroup);
            CreateSimulationRunner(simulationBackend);
            
            IScreenSwitcher screenSwitcher = CreateScreenSwitcher(_ttsScreen, _simulationScreen);
            StateMachine stateMachine = CreateAppStateMachine(simulationBackend, screenSwitcher, _ttsScreen, _simulationScreen.ScreenID);
            
            stateMachine.SetActiveState<StartMenuState<SimulationState>>().Forget(); // simple fire and forget kinda ok in that case

            // if some runtime content pre-loading is required(f.e content from addressables or configuration from network):
            // its better to move Initialization to specific State of a state machine
            // and show loading screen during initialization process 
            // state machine uses UniTask which makes most of the unity async api awaitable, addressables as well
        }

        private void CreateSimulationRunner(ISimulationBackend simulationBackend)
        {
            new GameObject("[Simulation Runner]")
                .AddComponent<SimulationRunner>()
                .Setup(simulationBackend)
                .transform.SetParent(this.transform);
        }
        
        public static ControllsGroup SetupControlls(UnityEngine.Object accelerationSlider, UnityEngine.Object cursorSizeSlider, UnityEngine.Object velocitySlider)
        {
            UISliderAdapter acceleration = new UISliderAdapter( accelerationSlider as ISlider);
            UISliderAdapter cursorSize = new UISliderAdapter(cursorSizeSlider as ISlider);
            UISliderAdapter speedSlider = new UISliderAdapter(velocitySlider as ISlider);
            ICursor cursor = new UnityCursor();
            return new ControllsGroup(cursor, cursorSize, acceleration, speedSlider);
        }

        private ISimulationBackend CreateSimulationBackend(ControllsGroup controllsGroup)
        {
            ISimulationBackendFactory simulationBackendFactory;
            if (_simulationBackend == SimulationBackend.Mono)
                simulationBackendFactory = new MonoSimulationBackendFactory(
                    _environmentConfig.Config,
                    _ballConfig.Config,
                    _cursorConfig.Config,
                    controllsGroup,
                    _sceneReferences);
            else
                simulationBackendFactory = new ECSSimulationBackendFactory(
                    _environmentConfig.Config,
                    _ballConfig.Config,
                    _cursorConfig.Config,
                    controllsGroup,
                    _sceneReferences
                );
            return simulationBackendFactory.CreateBackend();
        }

        public static StateMachine CreateAppStateMachine(ISimulationBackend simulationBackend, IScreenSwitcher screenSwitcher, TapToStartSreen ttsScreen, int simulationScreenID)
        {
            StateMachine stateMachine = new StateMachine();
            return stateMachine.RegisterStates(new IState[]
            {
                new StartMenuState<SimulationState>(screenSwitcher,ttsScreen, stateMachine),
                new SimulationState(simulationBackend, screenSwitcher, simulationScreenID)
            });
        }

        private IScreenSwitcher CreateScreenSwitcher(IUIScreen ttsScreen, IUIScreen simulationScreen)
        {
            return new ScreenSwitcher()
                .RegisterScreen(ttsScreen)
                .RegisterScreen(simulationScreen);
        }

#if UNITY_EDITOR
        private void OnValidate()
        {
            if (!ValidateType<ISlider>(_accelerationSlider))
                _accelerationSlider = null;

            if (!ValidateType<ISlider>(_cursorValueSlider))
                _cursorValueSlider = null;
            
            if (!ValidateType<ISlider>(_velocitySlider))
                _velocitySlider = null;
        }

        private bool ValidateType<T>(object obj)
        {
            if (!(obj is T) && !(obj is null))
            {
                if (obj is GameObject go)
                {
                    obj = go.GetComponentInChildren<T>();
                    if (obj != null)
                    {
                        return true;
                    }
                }
                
                Debug.LogError($"Object should implement type {typeof(T)}", this);
                return false;
            }

            return true;
        }
#endif
    }
}