using ATT.UI.Screens;
using Cysharp.Threading.Tasks;
using SimpleStateMachine;

namespace ATT.States
{
    // entry state waiting for user input to start simulation
    public class StartMenuState<NextStateT> : IState where NextStateT: IState
    {
        private IScreenSwitcher _screenSwitcher;
        private TapToStartSreen _ttsScreen;
        private StateMachine _stateMachine;

        public StartMenuState(IScreenSwitcher screenSwitcher, TapToStartSreen ttsScreen, StateMachine stateMachine)
        {
            _screenSwitcher = screenSwitcher;
            _ttsScreen = ttsScreen;
            _stateMachine = stateMachine;
        }

        public async UniTask OnEnter()
        {
            _ttsScreen.OnClickStart += TransitToNextState;
            await _screenSwitcher.SetScreen(_ttsScreen.ScreenID);
        }

        public async UniTask OnExit()
        {
            _ttsScreen.OnClickStart -= TransitToNextState;
        }

        private void TransitToNextState()
        {
            _stateMachine.SetActiveState<NextStateT>();
        }
    
    }
}