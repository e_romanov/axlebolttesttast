using ATT.Simulation.SimulationBackend;
using ATT.UI.Screens;
using Cysharp.Threading.Tasks;
using SimpleStateMachine;

namespace ATT.States
{
    // state when ball starts movement, after click on start button from menu
    public class SimulationState : IState
    {
        private ISimulationBackend _simulationBackend;
        private IScreenSwitcher _screenSwitcher;
        private int targetScreenID;

        public SimulationState(ISimulationBackend simulationBackend, IScreenSwitcher screenSwitcher, int targetScreenID)
        {
            _simulationBackend = simulationBackend;
            _screenSwitcher = screenSwitcher;
            this.targetScreenID = targetScreenID;
        }

        public async UniTask OnEnter()
        {
            await _screenSwitcher.SetScreen(targetScreenID);
            _simulationBackend.StartSimulation();
        }

        public async UniTask OnExit()
        {
        }
    }
}