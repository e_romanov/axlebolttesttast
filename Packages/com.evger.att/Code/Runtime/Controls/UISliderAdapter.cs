namespace ATT.Controls
{
    // to link ui slider with simulation control slider
    // its possible to just use some common interface, but with cost of increased coupling between packages
    public class UISliderAdapter : Simulation.Controlls.ISlider
    {
        private UI.Slider.ISlider _uiSlider;
        public float NormalizedValue { get => _uiSlider.NormalizedValue; set => _uiSlider.NormalizedValue = value;}

        public UISliderAdapter(UI.Slider.ISlider uiSlider)
        {
            _uiSlider = uiSlider;
        }
    }
}