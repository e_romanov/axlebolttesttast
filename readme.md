# AxleboltTestTask (ATT)

---
#### Requirements

- [Unity 2020.3.3f1 (LTS)](https://unity3d.com/ru/get-unity/download/archive)

- [git lfs](https://git-lfs.github.com) should be installed to pull/push content

- uses upm packages from [OpenUPM](https://openupm.com)

--- 

#### Included packages

**Unity official packages**

- [Cinemachine](https://docs.unity3d.com/Packages/com.unity.cinemachine@2.2/manual/index.html): to fit 3d view into any screen resolution on ATT scene

- [TextMeshPro](https://docs.unity3d.com/Packages/com.unity.textmeshpro@3.0/manual/index.html) 

**OpenUPM packages**

- [UniTask](https://openupm.com/packages/com.cysharp.unitask/)

- [Leopotam.Ecs](https://openupm.com/packages/com.leopotam.ecs/)

- [Leopotam.Ecs-unityIntegration](https://openupm.com/packages/com.leopotam.ecs-unityintegration/)

**Custom embeeded packages (all of them implemented for test task)**

- ATT - composition root

- ATT.UI - ui components 

- ATT.Simulation - base simulation components (common unity-views, interfaces and configurations)

- ATT.Simulation.ECS - ecs implementation of simulation

- ATT.Simultaion.Mono - classic monobehaviour based implementation

- SimpleStateMachine

---
#### Manual

Example scene: Assets/Scenes/ATT.unity

**How to switch simulation backend**:

Find [CompositionRoot] MonoBehaviour under [SceneRoot] in ATT scene

Make sure to exit playmode and change value of SimulationBackend switcher

![](Doc/Images/SimulationBackendMode.png)

---

#### Implementation details

Project implemented with upm packages workflow, there is no code under Assets folder. 

All code can be found under Packages folder

**ATT.UI** provides ui functionality.

**ATT.Simulation** provides common interfaces and configurations for Simulation backends

**ATT.Simulation.ECS** and **ATT.Simulation.Mono** are concrete implementations of simulation backends

**ATT.UI** and **ATT.Simulation** does not depends on each other and can be used separately. 

Interoperability of UI and Simulation solved on **ATT** package level. 

**ATT** package composes everything into single working solution.

For more info about upm-packages workflow relate to [unity documentation](https://docs.unity3d.com/Manual/CustomPackages.html)

---

If it is not clear how to write tests under ecs, approach similar to regular tests:

- prepare data (arrange)
    - crate ecs world
    - create ecs entities with pre configured components/data
    - create ecs systems required for test (may be just one system)
  
- execute ecs systems (act)
    - run ecs systems
  
- assert data from entities (assert)
    - for example check if some data inside component in entity satisfies conditions

---

#### Known issues

**Packages code analysis issues:**

There is cases when some IDEs cant analyze code from packages.

Solution: setup Generate .csproj files in Editor Settings and force unity to regenare it

![](Doc/Images/PackagesCodeAnalysisIssue.png)


**OpenUPM**:

If you network is under some strict vpn so that it cant acess OpenUPM servers to retrieve OpenUPM packages:

- checkout embedded-packages branch
